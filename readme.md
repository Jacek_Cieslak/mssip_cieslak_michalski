# HTTP SERVER
## Authors: Jacek Cieślak, Jacek Michalski
This project was created as an part of MSSiP (Microcontroler Systems of Conrol and Measurment) laboratories in Poznań University of Technology.
HTTP Server is running on STM32F107VC

IP: 192.168.0.10:80
Netmask: 255.255.255.0
Gateaway: 192.168.0.1

### How do I get set up? ###
Project is ready to use. To run it open MDK-ARM\testProject.uvprojx file using Keil uVision5. Compile and upload project to your developement board.
If you want to change network configuration adresses, you can do it in main.h file